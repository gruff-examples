import Control.Monad (ap)
import Data.Ratio ((%))
import System.Environment (getArgs)
import Fractal.GRUFF

main :: IO ()
main = do
  [scount, snum, sden] <- getArgs
  let count = read scount
      num = read snum
      den = read sden
      angle = num % den
      callback = RayTraceForwardCallback $ \continue c _ ->
         let x :+ y = toComplex c
         in  (if magnitudeSquared c < 4 then ((toDFloat x :+ toDFloat y) :) else id)
               (rayTraceForward continue callback)
      ray = rayTraceForward (rayTraceForwardStart (ExternalAngle angle)) callback
  defaultMain . map scene . zip [0..] . (zip`ap`tail) . take count $ ray

scene (n, (cx0 :+ cy0, cx1 :+ cy1)) =
  withDFloat cx0 $ \x0' -> withDFloat cy0 $ \y0' ->
    withDFloat cx1 $ \x1' -> withDFloat cy1 $ \y1 ->
      let x0 = auto x0'
          y0 = auto y0'
          x1 = auto x1'
          c0 = x0 :+ y0
          c1 = x1 :+ y1
          d = c1 - c0
          center' = 0.5 * (c0 + c1)
          radius' = 0.5 `min` (128 * auto (magnitude d) :: F24)
          orient' = phase (fmap realToFrac d :: Complex Double) - pi / 2
      in  (Image{ imageLocation = Location
                    { center = fmap toRational center'
                    , radius = toRational radius'
                    }
                , imageViewport = Viewport
                    { aspect = 512/288
                    , orient = orient'
                    }
                , imageWindow   = Window
                    { width = 512
                    , height =  288
                    , supersamples = 0.5
                    }
                , imageColours  = Colours
                    { colourInterior = Colour 1 0 0
                    , colourBoundary = Colour 0 0 0
                    , colourExterior = Colour 1 1 1
                    }
                , imageLabels   = []
                , imageLines    = []
                }
          , filename n)

filename n = (reverse . take 5 . (++ "00000") . reverse . show) n ++ ".ppm"
