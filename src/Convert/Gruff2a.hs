module Convert.Gruff2a (gruff2a) where
import Convert.Common (readMay, image', Colour(Colour), Image())
import Numeric (readSigned, readFloat)

data AngledInternalAddress
  = Unangled Integer
  | Angled Integer Angle AngledInternalAddress
  deriving Read

type Angle = Rational

newtype R = R Rational

instance Read R where
  readsPrec _ = map (\(x, s) -> (R x, s)) . readParen False (readSigned readFloat)

data Color = Color Int Int Int
  deriving Read

c :: Color -> Colour
c (Color r g b) = Colour (fromIntegral r / m) (fromIntegral g / m) (fromIntegral b / m) where m = 65535

data Gruff2a = Gruff2
  { gAddress :: Maybe AngledInternalAddress
  , gReal :: Maybe R
  , gImag :: Maybe R
  , gSize :: Maybe R
  , gRota :: Maybe Double
  , gColours :: (Color, Color, Color)
  }
  deriving Read

gruff2a :: String -> Maybe Image
gruff2a s = convert =<< readMay s

convert :: Gruff2a -> Maybe Image -- FIXME handle rota?
convert Gruff2
  { gReal = Just (R re), gImag = Just (R im), gSize = Just (R sz)
  , gColours = (ci, cb, ce)
  } = Just $ image' 512 288 4 (c ci, c cb, c ce) re im (fromRational sz)
convert _ = Nothing
