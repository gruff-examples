module Convert.Common (readMay, image, image', Colour(Colour), Image()) where
import Data.List (sortBy)
import Data.Ord (comparing)
import Fractal.GRUFF
import Fractal.RUFF.Types.Complex(Complex((:+)))

image :: Rational -> Rational -> Double -> Image
image = image' 512 288 4 (red, black, white)
  where
    red   = Colour 1 0 0
    black = Colour 0 0 0
    white = Colour 1 1 1

image' :: Int -> Int -> Double -> (Colour, Colour, Colour) -> Rational -> Rational -> Double -> Image
image' w h ss (ci, cb, ce) re im sz = Image
  { imageWindow = Window{ width = w, height = h, supersamples = ss }
  , imageViewport = Viewport{ aspect = fromIntegral w / fromIntegral h, orient = 0 }
  , imageLocation = Location{ center = re :+ im, radius = sz }
  , imageColours = Colours{ colourInterior = ci, colourBoundary = cb, colourExterior = ce }
  , imageLabels = []
  , imageLines = []
  }

readMay :: Read a => String -> Maybe a
readMay s = case sortBy (comparing (length . snd)) . filter (all whiteSpace . snd) . reads $ s of
  (a, _):_ -> Just a
  _ -> Nothing
  where
    whiteSpace ' '  = True
    whiteSpace '\t' = True
    whiteSpace '\n' = True
    whiteSpace '\r' = True
    whiteSpace  _   = False
