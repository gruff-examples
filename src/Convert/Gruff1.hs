module Convert.Gruff1 (gruff1) where
import Convert.Common (readMay, image, Image)
import Numeric (readSigned, readFloat)

data AngledInternalAddress
  = Unangled Integer
  | Angled Integer Angle AngledInternalAddress
  deriving Read

type Angle = Rational

newtype R = R Rational

instance Read R where
  readsPrec _ = map (\(x, s) -> (R x, s)) . readParen False (readSigned readFloat)

data Gruff1 = Gruff1
  { gAddress :: Maybe AngledInternalAddress
  , gIsland :: Maybe AngledInternalAddress
  , gChild :: Maybe [Angle]
  , gLowerAngle :: Maybe Angle
  , gUpperAngle :: Maybe Angle
  , gReal :: Maybe R
  , gImag :: Maybe R
  , gSize :: Maybe R
  , gHueShift :: Maybe R
  , gHueScale :: Maybe R
  }
  deriving Read

gruff1 :: String -> Maybe Image
gruff1 s = convert =<< readMay s

convert :: Gruff1 -> Maybe Image
convert Gruff1{ gReal = Just (R re), gImag = Just (R im), gSize = Just (R sz) } = Just $ image re im (fromRational sz)
convert _ = Nothing
