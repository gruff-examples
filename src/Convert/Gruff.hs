module Convert.Gruff (gruff) where
import Convert.Common (readMay, image, Image)
import Numeric (readSigned, readFloat)

data AngledInternalAddress
  = Unangled Integer
  | Angled Integer Angle AngledInternalAddress
  deriving Read

type Angle = Rational

newtype R = R Rational

instance Read R where
  readsPrec _ = map (\(x, s) -> (R x, s)) . readParen False (readSigned readFloat)

data Gruff = Gruff
  { gAddress :: Maybe AngledInternalAddress
  , gIsland :: Maybe AngledInternalAddress
  , gChild :: Maybe [Angle]
  , gLowerAngle :: Maybe Angle
  , gUpperAngle :: Maybe Angle
  , gReal :: Maybe R
  , gImag :: Maybe R
  , gSize :: Maybe R
  }
  deriving Read

gruff :: String -> Maybe Image
gruff s = convert =<< readMay s

convert :: Gruff -> Maybe Image
convert Gruff{ gReal = Just (R re), gImag = Just (R im), gSize = Just (R sz) } = Just $ image re im (fromRational sz)
convert _ = Nothing
