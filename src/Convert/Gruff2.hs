module Convert.Gruff2 (gruff2) where
import Convert.Common (readMay, image', Colour(Colour), Image())
import Numeric (readSigned, readFloat)

data AngledInternalAddress
  = Unangled Integer
  | Angled Integer Angle AngledInternalAddress
  deriving Read

type Angle = Rational

newtype R = R Rational

instance Read R where
  readsPrec _ = map (\(x, s) -> (R x, s)) . readParen False (readSigned readFloat)

data Color = Color Int Int Int
  deriving Read

c :: Color -> Colour
c (Color r g b) = Colour (fromIntegral r / m) (fromIntegral g / m) (fromIntegral b / m) where m = 65535

data Window = Window{ width :: Int, height :: Int, supersamples :: R }
  deriving Read

data Viewport = Viewport{ aspect :: R, orient :: R }
  deriving Read

data Gruff2 = Gruff2
  { gAddress :: Maybe AngledInternalAddress
  , gReal :: Maybe R
  , gImag :: Maybe R
  , gSize :: Maybe R
  , gRota :: Maybe Double
  , gColours :: (Color, Color, Color)
  , gWindow :: Window
  , gViewport :: Viewport
  }
  deriving Read

gruff2 :: String -> Maybe Image
gruff2 s = convert =<< readMay s

convert :: Gruff2 -> Maybe Image -- FIXME handle rota, orient, aspect?
convert Gruff2
  { gReal = Just (R re), gImag = Just (R im), gSize = Just (R sz)
  , gColours = (ci, cb, ce)
  , gWindow = Window{ width = w, height = h, supersamples = R ss }
  } = Just $ image' w h (fromRational ss) (c ci, c cb, c ce) re im (fromRational sz)
convert _ = Nothing
