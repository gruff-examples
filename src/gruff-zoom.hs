import System.Environment (getArgs)

import Fractal.GRUFF

main :: IO ()
main = do
  [fn, nfs] <- getArgs
  i0 <- read `fmap` readFile fn
  let nf = read nfs
      r0 = 4
      r1 = radius (imageLocation i0)
      dr = r1 / r0
      zoom f =
        let t = fromIntegral f / fromIntegral (nf - 1)
            r = r0 * dr ** t
        in  i0{ imageLocation = (imageLocation i0){ radius = r } }
      name = (++ ".ppm") . reverse . take 8 . (++ "00000000") . reverse . show
  defaultMain [ (zoom f, name f) | f <- [0 .. nf - 1] ]
