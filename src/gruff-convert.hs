import Data.Maybe (mapMaybe)
import System.Environment (getArgs)
import System.FilePath ((</>), takeFileName)

import Convert.Common (Image)
import Convert.Gruff  (gruff)
import Convert.Gruff1 (gruff1)
import Convert.Gruff2a(gruff2a)
import Convert.Gruff2 (gruff2)

parsers :: [String -> Maybe Image]
parsers = [gruff2, gruff2a, gruff1, gruff]

main :: IO ()
main = do
  args <- getArgs
  case args of
    odir:files@(_:_) -> mapM_ (main1 odir) files
    _ -> putStrLn "usage: gruff-convert outdir/ *.oldformat.gruff"

main1 :: FilePath -> FilePath -> IO ()
main1 odir file = do
  old <- readFile file
  case mapMaybe ($ old) parsers of
    []    -> putStrLn $ "Error: `" ++ file ++ "' unrecognised"
    [new] -> writeFile (odir </> takeFileName file) (show new)
    _     -> putStrLn $ "Error: `" ++ file ++ "' ambiguous"
